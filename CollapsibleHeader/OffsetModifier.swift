//
//  OffsetModifier.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 28/03/2022.
//

import SwiftUI

// To get scroll Offset...
struct OffsetModifier: ViewModifier {

    @Binding var offset: CGFloat
    @State var startOffset: CGFloat = 0

    func body(content: Content) -> some View {
        content
            .overlay(
                GeometryReader{proxy in
                    Color.clear
                        .preference(key: OffsetKey.self, value: proxy.frame(in: .global).minY)
                }
            )
            .onPreferenceChange(OffsetKey.self) { offset in
                if startOffset == 0{
                    startOffset = offset
                }
                self.offset = startOffset - offset
            }
    }
}

// Creating ScrollOffset Prefrence...
struct OffsetKey: PreferenceKey{
    static var defaultValue: CGFloat = 0

    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}




// Extending view to get screen size and safe area insect...
extension View{

    func getScreenBound()->CGRect{
        return UIScreen.main.bounds
    }

    func getSafeArea()->UIEdgeInsets{

        let null = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        guard let screen = UIApplication.shared.connectedScenes.first as? UIWindowScene else{
            return null
        }

        guard let safeArea = screen.windows.first?.safeAreaInsets else{
            return null
        }

        return safeArea
    }
}
