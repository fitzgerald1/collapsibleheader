//
//  AccountList.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 16/05/2022.
//

import SwiftUI

struct AccountList: View {
    var body: some View {
        NavigationView {
            NavigationLink(destination: DuplicateView()) {
                Text("Next View")
            }
        }
    }
}

struct AccountList_Previews: PreviewProvider {
    static var previews: some View {
        AccountList()
    }
}
