//
//  ViewModel.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 29/03/2022.
//

import Foundation
import SwiftUI


protocol Activatable: AnyObject {
     func activate()
 }

 extension View {
     func activating(_ object: Activatable) -> some View {
         onAppear(perform: object.activate)
     }
 }


class ViewModel: ObservableObject, Activatable {
    @Published var password: String?
    @Published var email: String?
    @Published var username: String? {
        didSet {
            if username?.isEmpty == true {
                username = nil
            }
        }
    }

    func activate() {
    }

    func setInitialValues(_ value: String) {
        password = value
        email = value + "@gmail.com"
        username = "@" + value
    }

    func deactivate(){
        password = nil
        email = nil
        username = nil
    }
}

