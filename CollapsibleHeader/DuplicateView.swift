//
//  HomeView.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 29/03/2022.
//

import SwiftUI

struct DuplicateView: View {
    @EnvironmentObject var viewModel: ViewModel

    // Offset...
    @State var offset: CGFloat = 0

    // Constants
    var headerHeight: CGFloat = 165
    var maxScrollOffset: CGFloat = 95
    var maxProgress = 0.4
    var topHeight: CGFloat { maxScrollOffset - getSafeArea().top }

    var collapsedIconWidth: CGFloat = 55
    var collapsedIconHeight: CGFloat {
        collapsedIconWidth
    }
    var textWidth: CGFloat {
        (viewModel.username ?? "New Account Name").widthOfString(usingFont: UIFont.systemFont(ofSize: 28))
    }
    var collapsedIconPadding: CGFloat = 5
    var collapsedIconOffsetX: CGFloat {
        -textWidth / 2.0 - collapsedIconWidth / 2.0 - collapsedIconPadding
    }

    var collapsedIconOffsetY: CGFloat {
        // magic value
        21
    }

    var interpolatedIconOffsetX: CGFloat {
        (progress * collapsedIconOffsetX) + (1.0 - progress) * defaultIconOffsetX
    }

    var interpolatedIconOffsetY: CGFloat {
        (progress * collapsedIconOffsetY) + (1.0 - progress) * defaultIconOffsetY
    }

    var interpolatedIconWidth: CGFloat {
        (progress * collapsedIconWidth) + (1.0 - progress) * defaultIconWidth
    }

    var interpolatedIconHeight: CGFloat {
        interpolatedIconWidth
    }

    var defaultIconOffsetY: CGFloat {
        120
    }

    var defaultIconOffsetX: CGFloat {
        0
    }

    var defaultIconWidth: CGFloat {
        120
    }
    var progress: CGFloat {
        getRelativeScrollProgress()
    }

    var body: some View {

        ZStack(alignment: .top) {

            VStack {
                ZStack {
                    Rectangle()
                        .foregroundColor(.blue)
                        .frame(height: headerHeight)

                    Image("header")
                        .resizable()
                        .scaledToFill()
                        .frame(height: headerHeight)
                        .clipped()
                }
                .offset(y:  0 - getCurrentOffset())


                VStack {
                    ZStack {
                        FormHeaderTextField("New Account Name", value: $viewModel.username)
                            .padding(.horizontal, 45)
                            //.border(Color.black)
                            //.frame(width: (viewModel.username ?? "New Account Name").widthOfString(usingFont: UIFont.systemFont(ofSize: 28)), alignment: .center)
                            //.frame(width: $viewModel.username.widthOfString(usingFont: .systemFont(ofSize: 28)))
                            //.offset(x: 0 + (progress * 60))

                        HStack {
                            Button(action: {}) {
                                Image(systemName: "chevron.left")
                                    .resizable()
                                    .font(.title.weight(.medium))
                                    .foregroundColor(.white)
                                    .frame(width: 12, height: 20)
                                    .padding(18)
                            }

                            Spacer()
                        }
                        .frame(height: 50)
                    }
                }
                .frame(width: getScreenBound().width)
                .offset(y: -120 - (progress * 30))
            }
            .zIndex(1)

            Image("icon")
                .resizable()
                .scaledToFill()
                //.frame(width: 120 - (80 * progress), height: 120 - (80 * progress))
                .frame(width: interpolatedIconWidth, height: interpolatedIconHeight)
                .clipped()
                .overlay(
                    RoundedRectangle(cornerRadius: 16)
                        .stroke(Color.gray, lineWidth: 1)
                )
                .offset(x: interpolatedIconOffsetX, y: interpolatedIconOffsetY)
                .zIndex(2)
            //(getScreenBound().width - textWidth) / 2 - 60
                //.offset(x: 0 - (progress * 90), y: 0 - (progress * 55))

            ScrollView {
                VStack {
                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username 1")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username 2")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username 3")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username 4")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username 5")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)
                }
                .frame(width: getScreenBound().width - 32)
                .padding(.horizontal)
                .padding(.top, 270)
                .padding(.top,-getSafeArea().top)
                .modifier(OffsetModifier(offset: $offset))
            }
        }
        .ignoresSafeArea(.container, edges: .top)
        .environmentObject(viewModel)
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }









    // MARK: Scroll offset methods
    /// returns absolute amount scrolled by the scrollview
    func getCurrentOffset()->CGFloat{
        // stopping at when navbar size at 70....
        print("Scroll Offset: \(offset)")
        let scrollOffset = offset > topHeight ? topHeight : offset
        return scrollOffset < 0 ? 0 : scrollOffset
    }

    /// returns relative amount scrolled by the scrollview to the topHeight
    func getRelativeScrollProgress()->CGFloat{
        let currentOffset = getCurrentOffset()
        let progress = min(1.0, currentOffset / topHeight)
        //assert(progress >= 0)
        print("Progress: \(progress)")
        return progress
    }
}


extension String {
   func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}
