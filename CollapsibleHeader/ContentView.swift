//
//  ContentView.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 28/03/2022.
//


import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            AccountList()
                .environmentObject(ViewModel())
                .tabItem {
                    Label("Accounts", image: "accounts")
                }

            SettingsView()
                .tabItem {
                    Label("Settings", image: "settings")
                }
        }
    }
}
