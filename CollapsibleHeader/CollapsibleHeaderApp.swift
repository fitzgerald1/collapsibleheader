//
//  CollapsibleHeaderApp.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 28/03/2022.
//

import SwiftUI

@main
struct CollapsibleHeaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
