//
//  HomeView.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 29/03/2022.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject var viewModel: ViewModel

    // Offset...
    @State var offset: CGFloat = 0

    // Constants
    var headerHeight: CGFloat = 250
    var maxScrollOffset: CGFloat = 180
    var maxProgress = 0.4

    var body: some View {

        ZStack(alignment: .top) {
            ZStack{
                VStack {
                    Image("header")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: getScreenBound().width, alignment: .bottom)
                        .clipped()
                        .background(Color.blue)
                }

                let progress = -getProgress() < maxProgress ? getProgress() : -(maxProgress)

                VStack(alignment: .center, spacing: 15) {
                    Text("Netflix")
                        .font(.title)
                        .multilineTextAlignment(.center)
                        .scaleEffect(1 + progress,anchor: .bottomTrailing)
                        .offset(x: 1 + progress * -40, y: progress * -120)

                    Image("icon")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipShape(RoundedRectangle(cornerRadius: 16))
                        .scaleEffect(1 + progress * 1.3, anchor: .topLeading)
                }
                .padding(15)
                .padding(.bottom, 30)
                // stopping view at bottom...
                // max Progress = 0.4
                // 0.4 * 200 = 80
                // topbar height = 70
                // 10 = padding....
                .offset(y: progress * -200)
                .frame(maxWidth: .infinity,maxHeight: .infinity,alignment: .bottom)
            }
            .frame(height: headerHeight)
            .offset(y: getOffset())
            .zIndex(1)

            ScrollView {
                VStack {
                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)

                    Group {
                        Text("E-mail or username")
                            .frame(maxWidth: .infinity, alignment: .leading)

                        HStack {
                            UsernameField(
                                "Add your email or username",
                                value: $viewModel.username)
                        }
                    }
                    .padding(.bottom)
                }
                .padding(.horizontal)
                .padding(.top, headerHeight)
                .modifier(OffsetModifier(offset: $offset))
            }
            .padding(.top)

        }
        .ignoresSafeArea(.container, edges: .top)
        .environmentObject(viewModel)
    }

    func getOffset()->CGFloat{
        // stopping at when navbar size at 70....
        let checkSize = -offset < (maxScrollOffset - getSafeArea().top) ? offset : -(maxScrollOffset - getSafeArea().top)
        return offset < 0 ? checkSize : 0
    }

    // progress...
    func getProgress()->CGFloat{
        let topheight = (maxScrollOffset - getSafeArea().top)
        return getOffset() / topheight
    }
}
