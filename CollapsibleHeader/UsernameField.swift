//
//  UsernameField.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 29/03/2022.
//

import SwiftUI


struct UsernameField: View {
    var label: String
    @Binding var value: String?
    var onEditingChanged: (Bool) -> Void = { _ in }
    @State var focused: Bool = false

    init(
        _ label: String,
        value: Binding<String?>,
        onEditingChanged: @escaping (Bool) -> Void = { _ in }
    ) {
        self.label = label
        self._value = value
        self.onEditingChanged = onEditingChanged
    }

    var body: some View {
        TextField(label, text: Binding<String>(
            get: { self.value ?? "" },
            set: { self.value = $0 }
        ), onEditingChanged: { edit in
            focused = edit
            onEditingChanged(edit)
        })
        .disableAutocorrection(true)
        .keyboardType(.emailAddress)
        .autocapitalization(.none)
        .padding()
        .frame(height: 48)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color.black, lineWidth: 1)
        )
    }
}
