//
//  FormHeaderTextField.swift
//  CollapsibleHeader
//
//  Created by Fitzgerald Afful on 30/03/2022.
//

import SwiftUI


struct FormHeaderTextField: View {
    var label: String
    @Binding var value: String?
    var onEditingChanged: (Bool) -> Void = { _ in }
    @State var focused: Bool = false
    @State var isPlaceholderHidden = false

    init(_ label: String, value: Binding<String?>, onEditingChanged: @escaping (Bool) -> Void = { _ in }) {
        self.label = label
        self._value = value
        self.onEditingChanged = onEditingChanged
    }

    var body: some View {
        ZStack {
            if !isPlaceholderHidden {
                Text(label)
                    .font(Font.title)
                    .foregroundColor(.white.opacity(0.5))
            }

            FocussableTextField(text: Binding<String>(
                get: { self.value ?? "" },
                set: {
                    self.value = $0
                    isPlaceholderHidden = !value.isEmptyOrNil
                }
            ), isFirstResponder: $focused) {
                $0.autocorrectionType = .no
                $0.font = UIFont.systemFont(ofSize: 28)
                $0.textAlignment = .center
                $0.textColor = .white
                $0.tintColor = .white
            }
            .frame(height: 50)
            .onAppear {
                // autofocus on new account name field first
                // when creating a new account
                if value == nil {
                    // delay autofocus animation
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        focused = true
                    }
                } else {
                    isPlaceholderHidden = true
                }
            }
        }
    }
}




// Focussable input for iOS 13 & 14
// Source: https://stackoverflow.com/a/59059359
struct FocussableTextField: UIViewRepresentable {
    @Binding public var isFirstResponder: Bool
    @Binding public var text: String

    public var configuration = { (_: UITextField) in }

    public init(text: Binding<String>, isFirstResponder: Binding<Bool>, configuration: @escaping (UITextField) -> Void = { _ in }) {
        self.configuration = configuration
        self._text = text
        self._isFirstResponder = isFirstResponder
    }

    public func makeUIView(context: Context) -> UITextField {
        let view = UITextField()
        view.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        view.addTarget(context.coordinator, action: #selector(Coordinator.textViewDidChange), for: .editingChanged)
        view.delegate = context.coordinator
        return view
    }

    public func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
        configuration(uiView)
        DispatchQueue.main.async {
            switch isFirstResponder {
            case true: uiView.becomeFirstResponder()
            case false: uiView.resignFirstResponder()
            }
        }
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator($text, isFirstResponder: $isFirstResponder)
    }

    public class Coordinator: NSObject, UITextFieldDelegate {
        var text: Binding<String>
        var isFirstResponder: Binding<Bool>

        init(_ text: Binding<String>, isFirstResponder: Binding<Bool>) {
            self.text = text
            self.isFirstResponder = isFirstResponder
        }

        @objc public func textViewDidChange(_ textField: UITextField) {
            self.text.wrappedValue = textField.text ?? ""
        }

        public func textFieldDidBeginEditing(_ textField: UITextField) {
            self.isFirstResponder.wrappedValue = true
        }

        public func textFieldDidEndEditing(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.isFirstResponder.wrappedValue = false
            }
        }

        // Added this myself. In most cases we want to resign input on return
        public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.isFirstResponder.wrappedValue = false
            return false
        }
    }
}


extension Optional where Wrapped == String {
    // extension to check whether a String is empty or nil
    var isEmptyOrNil: Bool {
        return self?.isEmpty ?? true
    }
}
